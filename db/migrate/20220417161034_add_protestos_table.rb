class AddProtestosTable < ActiveRecord::Migration[7.0]
  def change
    create_table :protestos do |t|
      t.string :cnpj, null: false, index: true
      t.string :tabeliao, null: false, index: true
      t.integer :valor, null: false
      t.datetime :vencimento, null: false
      t.timestamps
    end
  end
end
