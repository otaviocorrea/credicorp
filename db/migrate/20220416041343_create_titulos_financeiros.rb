class CreateTitulosFinanceiros < ActiveRecord::Migration[7.0]
  def change
    create_table :titulos_financeiros do |t|
      t.string :cnpj_cedente, null: false, index: true
      t.string :cnpj_sacado, null: false, index: true
      t.string :numero, null: false
      t.integer :valor, null: false
      t.boolean :titulo_ja_registrado, null: true
      t.datetime :vencimento, null: false
      t.timestamps
    end
  end
end
