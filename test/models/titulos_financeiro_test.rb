require "test_helper"

class TitulosFinanceiroTest < ActiveSupport::TestCase
  fixtures :titulos_financeiros
  titulo_params = {cnpj_sacado: '00.000.000/0000-00', cnpj_cedente: '00.000.000/0000-00', numero: '123456789012345', valor: 100, vencimento: 1.day.after}
  
  test "create if parameters are correct" do
    titulo = TitulosFinanceiro.new(titulo_params)
    assert_equal(true, titulo.save)
  end

  test "dont save if cnpj_cedente is invalid or blank" do
    titulo = titulos_financeiros(:one)
    titulo.cnpj_cedente = '000.00.000/0000-00'
    assert_equal(false, titulo.save)
    titulo.cnpj_cedente = nil
    assert_equal(false, titulo.save)
  end

  test "dont save if cnpj_sacado is invalid or blank" do
    titulo = titulos_financeiros(:one)
    titulo.cnpj_sacado = '000.00.000/0000-00'
    assert_equal(false, titulo.save)
    titulo.cnpj_sacado = nil
    assert_equal(false, titulo.save)
  end

  test "dont save if numero is invalid or blank" do
    titulo = titulos_financeiros(:one)
    titulo.numero = '123456789012345-'
    assert_equal(false, titulo.save)
    titulo.numero = nil
    assert_equal(false, titulo.save)
  end

  test "dont save if valor is invalid or blank" do
    titulo = titulos_financeiros(:one)
    titulo.valor = 0
    assert_equal(false, titulo.save)
    titulo.valor = nil
    assert_equal(false, titulo.save)
  end

  test "dont save if vencimento is invalid or blank" do
    titulo = titulos_financeiros(:one)
    titulo.vencimento = '2020-01-01'
    assert_equal(false, titulo.save)
    titulo.vencimento = '202033-0d1-01d'
    assert_equal(false, titulo.save)
    titulo.vencimento = nil
    assert_equal(false, titulo.save)
  end

end
