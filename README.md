## ⚠️ Requirements
* Ruby (v3.1.1)
* PostgreSQL (v12.9)
* Redis-server (v5.0.7)

## 📦 Run project
```bash
# Install gems
λ bundle install

# Create database and run migrations
λ rails db:create db:migrate

# Run Sidekiq , I recommend it to be in a parallel terminal 
λ bundle exec sidekiq

# Start server
λ rails s
```

## Tests
```bash
λ rails test
```


## 📜 Endpoints to use API
> GET => /titulos_financeiros

  * Parametros
    * cnpj_cedente* => 00.000.000/0001-00
    * cnpj_sacado => 00.000.000/0001-00
    * numero  => Pode ser apenas uma parte do numero
    * vencimento => AAAA-MM-DD
    * vencimento_fim => AAAA-MM-DD, se passado, junto ao parametro vencimento, filtra o período de datas
  
  ##### \* Required

  ex:
  ```bash
    # GET =>
    http://localhost:3000/titulos_financeiros?cnpj_cedente=00.000.000/0001-00
  ```
  ```json
    // RESPONSE =>
    [
      {
        "id": 6,
        "cnpj_cedente": "00.000.000/0001-00",
        "cnpj_sacado": "00.000.000/0001-90",
        "numero": "1222dsf3v",
        "valor": 1,
        "titulo_ja_registrado": false,
        "vencimento": "2022-10-25T00:00:00.000Z",
        "created_at": "2022-04-18T14:08:49.883Z",
        "updated_at": "2022-04-18T14:08:49.883Z",
        "cedente_protestos": [
          {
            "id": 1,
            "cnpj": "00.000.000/0001-00",
            "tabeliao": "Tabelião 28",
            "valor": 417848,
            "vencimento": "2022-02-14T00:00:00.000Z",
            "created_at": "2022-04-18T14:04:56.510Z",
            "updated_at": "2022-04-18T14:04:56.510Z"
          }
        ],
        "sacado_protestos": []
      }
    ]   
  ```

  
    

> POST => /titulos_financeiros/import
```json
{
  "titulos_financeiros": [
    {
      "cnpj_cedente": "00.000.000/0001-34", // Formato => 00.000.000/0001-00
      "cnpj_sacado": "00.000.000/0001-90", // Formato => 00.000.000/0001-00
      "numero": "1222dsf3v", // Apenas numeros e letras
      "valor": 1, //Integer => Deve ser maior que 0
      "vencimento": "2022-10-25" //AAAA-MM-DD => Deve ser maior que hoje
    }
  ]
}
```
