class TitulosFinanceirosController < ApplicationController
  skip_before_action :verify_authenticity_token

  def index
    begin 
      @titulos_financeiros = TitulosFinanceiro.filter_cnpj_cedente(params[:cnpj_cedente])
      .eager_load(:cedente_protestos).eager_load(:sacado_protestos)
      apply_index_filters
    end
    render json: index_params[:cnpj_cedente].present? ? 
      @titulos_financeiros : 
      { :message => "the url parameter 'cnpj_cedente' is required" }
  end

  def import
    invalids = []
    
    import_params.each do |titulo_params|
      titulo = TitulosFinanceiro.new(titulo_params)
      invalids.push(titulo_params.merge({:errors => titulo.errors})) unless titulo.save
    end

    render json: {
      :message => "#{import_params.count - invalids.count} titulos registrados com sucesso!",
      :invalids => invalids
    }
  end

  private

  def import_params
    params.permit(
      :titulos_financeiros => [
        :cnpj_cedente, :cnpj_sacado, :numero, :valor, :vencimento 
      ]
    )[:titulos_financeiros]
  end

  def index_params
    params.permit(:cnpj_cedente, :cnpj_sacado, :vencimento, :vencimento_fim, :numero)
  end

  def apply_index_filters
    @titulos_financeiros = @titulos_financeiros.filter_cnpj_sacado(index_params[:cnpj_sacado]) if index_params[:cnpj_sacado].present?
    @titulos_financeiros = @titulos_financeiros.filter_vencimento(index_params[:vencimento]) if index_params[:vencimento].present? && !index_params[:vencimento_fim].present?
    @titulos_financeiros = @titulos_financeiros.filter_vencimento_periodo(index_params[:vencimento], index_params[:vencimento_fim]) if index_params[:vencimento].present? && index_params[:vencimento_fim].present? 
    @titulos_financeiros = @titulos_financeiros.filter_numero(index_params[:numero]) if index_params[:numero].present?
  end
end
