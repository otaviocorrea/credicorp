class VerificationTitulosFinanceirosJob < ApplicationJob
  queue_as :default

  def perform(titulo_id)
    @titulo = TitulosFinanceiro.find(titulo_id)
    return nil unless @titulo
    verifica_e_grava_protestos(@titulo.cnpj_cedente) rescue nil
    verifica_e_grava_protestos(@titulo.cnpj_sacado) rescue nil
    verifica_registro_titulo rescue nil
  end

  def verifica_e_grava_protestos(cnpj)
    protestos = Integration::VerificaProtesto.request(cnpj) rescue []
    protestos.each do |protesto|
      Protesto.create(cnpj: cnpj, tabeliao: protesto["tabeliao"], valor: protesto["valor"], vencimento: protesto["data"])
    end
  end

  def verifica_registro_titulo
    status_registro = Integration::VerificaEmissao.request(@titulo.numero) rescue nil
    return nil if status_registro.nil?
    @titulo.update_column(:titulo_ja_registrado, (status_registro["status"].present? && status_registro["status"] == "registrado"))
  end
end
