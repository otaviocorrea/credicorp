class Protesto < ApplicationRecord
  validates :cnpj, { :presence => true, :format => { :with => VALID_CNPJ_REGEX } }
  validates :tabeliao, { :presence => true }
  validates :valor, { :presence => true, numericality: { only_integer: true, greater_than: 0 } }
  validates :vencimento, { :presence => true }
end