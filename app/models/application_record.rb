class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  VALID_CNPJ_REGEX = /\A\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}\z/
  VALID_DATE_REGEX = /\A\d{4}\-\d{2}\-\d{2}\z/
  VALID_NUMERO_TITULO_REGEX = /\A[a-z0-9]+\z/i
end
