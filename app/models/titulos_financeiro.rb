class TitulosFinanceiro < ApplicationRecord
  after_create :iniciar_verificacao_protestos_e_registro

  has_many :cedente_protestos, foreign_key: :cnpj, primary_key: :cnpj_cedente, class_name: "Protesto"
  has_many :sacado_protestos, foreign_key: :cnpj, primary_key: :cnpj_sacado, class_name: "Protesto"

  accepts_nested_attributes_for :cedente_protestos
  accepts_nested_attributes_for :sacado_protestos

  validates :cnpj_cedente, { :presence => true, :format => { :with => VALID_CNPJ_REGEX } }
  validates :cnpj_sacado, { :presence => true, :format => { :with => VALID_CNPJ_REGEX } }
  validates :numero, { 
            :presence => true, 
            :uniqueness => { scope: :cnpj_cedente, message: "There is already a 'titulo' with this 'numero' for this 'cedente'" }, 
            :format => { :with => VALID_NUMERO_TITULO_REGEX } 
          }
  validates :valor, { :presence => true, numericality: { only_integer: true, greater_than: 0 } }
  validates :vencimento, { :presence => true }
  validate :vencimento_is_date?
  validate :vencimento_greater_than_today?

  scope :filter_cnpj_cedente, -> (cnpj) { where cnpj_cedente: cnpj }
  scope :filter_cnpj_sacado, -> (cnpj) { where cnpj_sacado: cnpj }
  scope :filter_vencimento, -> (data) { where vencimento: data }
  scope :filter_vencimento_periodo, -> (data_inicio, data_fim) { where vencimento: data_inicio..data_fim }
  scope :filter_numero, -> (numero) { where("numero LIKE ?", "%#{numero}%") }
  
  def as_json(options={})
    super(options.merge(include: [:cedente_protestos, :sacado_protestos]))
  end
  private
  def vencimento_is_date?
    return true if vencimento.is_a?(ActiveSupport::TimeWithZone)
    errors.add(:vencimento, 'must be a valid date (YYYY-MM-DD)')
    false
  end

  def vencimento_greater_than_today?
    return true if vencimento.is_a?(ActiveSupport::TimeWithZone) && vencimento > Date.today
    errors.add(:vencimento, "must be greater than today's date")
    false
  end

  def iniciar_verificacao_protestos_e_registro
    VerificationTitulosFinanceirosJob.perform_later(id)
  end
end
