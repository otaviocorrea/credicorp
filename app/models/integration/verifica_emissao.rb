require 'net/http'
require 'uri'

module Integration
  class VerificaEmissao
    API = "https://rgshxukw2a.execute-api.us-east-1.amazonaws.com/default/validadorRegistroTituloFake?numero_titulo=" #numero_tiluto => ab123CDahweu123
    def initialize(numero_titulo)
      @numero_titulo = numero_titulo
    end

    def self.headers
      {
        'User-Agent' => 'MyApp (yourname@example.com)',
      }
    end

    def self.request(numero_titulo = @numero_titulo)
      @numero_titulo ||= numero_titulo
      url = URI.parse(API + @numero_titulo)
      response = Net::HTTP.get_response(url, headers)
      body = response.body.encode("UTF-8", invalid: :replace, undef: :replace)
      JSON.parse(body)
    end
  end
end