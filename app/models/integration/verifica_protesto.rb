require 'net/http'
require 'uri'

module Integration
  class VerificaProtesto
    API = "https://6e3v4cnk5i.execute-api.us-east-1.amazonaws.com/default/validatorProtestoFake?cnpj=" #CNPJ => 00.000.000/0000-00
    def initialize(cnpj)
      @cnpj = cnpj
    end

    def self.headers
      {
        'User-Agent' => 'MyApp (yourname@example.com)',
      }
    end

    def self.request(cnpj = @cnpj)
      @cnpj ||= cnpj
      url = URI.parse(API + @cnpj)
      response = Net::HTTP.get_response(url, headers)
      body = response.body.encode("UTF-8", invalid: :replace, undef: :replace)
      JSON.parse(body)
    end
  end
end