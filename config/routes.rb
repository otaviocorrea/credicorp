require 'sidekiq/web'

Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  mount Sidekiq::Web => '/sidekiq'

  resource :titulos_financeiros do 
    get '/' => 'titulos_financeiros#index'
    get '/show/:numero' => 'titulos_financeiros#show'
    post 'import' => 'titulos_financeiros#import'
  end
end
